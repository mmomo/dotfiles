Add this lines to /etc/profile

if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
   exec sway
fi

This allow you to execute sway when you log in.