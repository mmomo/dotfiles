#!/usr/bin/sh

FILENAME=scrn-$(date +"%Y-%m-%d-%H-%M-%S").png

slurp | grim -g - ~/Pictures/screenshots/$FILENAME

~/.config/sway/notify.py --icon CAMERA --title "Screenshot" --msg "$FILENAME"
