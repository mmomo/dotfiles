#!/usr/bin/python3

import os
import sys
import argparse
from enum import Enum

class Icon(Enum):
    POWER = "/usr/share/icons/elementary/categories/48/preferences-system-power.svg"
    INFO = "/usr/share/icons/elementary/status/48/dialog-information.svg"
    HELP = "/usr/share/icons/elementary/categories/48/system-help.svg"
    OK = "/usr/share/icons/elementary/emblems/48/emblem-default.svg"
    CONFIG = "/usr/share/icons/elementary/apps/48/application-default-icon.svg"
    BLUETOOTH = "/usr/share/icons/elementary/devices/48/bluetooth.svg"
    CAMERA= "/usr/share/icons/elementary/devices/48/camera.svg"
    MONITOR = "/usr/share/icons/elementary/devices/48/system.svg"
    WIRELESS= "/usr/share/icons/elementary/devices/48/network-wireless.svg"
    RELOAD = "/usr/share/icons/elementary/actions/48/system-restart.svg"
    EMPTY = ""
    
parser = argparse.ArgumentParser()

parser.add_argument('--icon', type=str, required=False)
parser.add_argument('--title', type=str, required=False)
parser.add_argument('--msg', type=str, required=True)
parser.add_argument('--critical', type=str, required=False)
args = parser.parse_args()


title = ""
text = ""
img = Icon.OK

title = args.title
text = args.msg

if args.icon == "POWER":
    img = Icon.POWER.value
elif args.icon == "INFO":
    img = Icon.INFO.value
elif args.icon == "HELP":
    img = Icon.HELP.value
elif args.icon == "OK":
    img = Icon.OK.value
elif args.icon == "CONFIG":
    img = Icon.CONFIG.value
elif args.icon == "BLUETOOTH":
    img = Icon.BLUETOOTH.value
elif args.icon == "CAMERA":
    img = Icon.CAMERA.value
elif args.icon == "MONITOR":
    img = Icon.MONITOR.value
elif args.icon == "WIRELESS":
    img = Icon.WIRELESS.value
elif args.icon == "RELOAD":
    img = Icon.RELOAD.value
elif args.icon == "EMPTY":
    img = Icon.EMPTY.value
else:
    img = Icon.EMPTY.value


base_cmd = "notify-send "
if args.critical == "true" or args.critical == "True" or args.critical == "TRUE":
    print("critical")
    base_cmd += "-u critical "


cmd = ""

if not title:
    cmd = "-h string:image-path:{img} '{txt}'".format(img=img, txt=text)
    base_cmd += cmd
else:
    cmd = "-h string:image-path:{img} '{title}' '{txt}'".format(img=img, title=title, txt=text)
    base_cmd += cmd

os.system(base_cmd)

