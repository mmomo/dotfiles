# Disable touchscreen

* Create the file
  /etc/udev/rules.d/80-touchscreen.rules

SUBSYSTEM=="usb", ATTRS{idVendor}=="xxxx", ATTRS{idProduct}=="xxxx", ATTR{authorized}="0"

you can find idVendor and idProduct with the command:

  cat /proc/bus/input/devices

* Reload the rules
  sudo udevadm control --reload
  sudo udevadm trigger
